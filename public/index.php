<?php

use App\Models\BillItems;
use App\Models\Bills;
use App\Models\Items;
use App\Models\States;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Selective\BasePath\BasePathMiddleware;
use Slim\Factory\AppFactory;
use App\Models\Tools;


require_once __DIR__ . '/../vendor/autoload.php';
$app = AppFactory::create();
$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();
$app->addErrorMiddleware(true, true, true);
$app->add(new BasePathMiddleware($app));

$app->get('/', function (Request $request, Response $response) {
    $response->getBody()->write(json_encode(['error' => false, 'message' => 'Welcome to DEMO-IIKO app!'], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});

$app->get('/init', function (Request $request, Response $response) use ($tools) {
    try {
        Tools::initDB();
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});

$app->get('/states', function (Request $request, Response $response, array $args) {
    try {
        $states = new States();
        $list = $states->list();
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'list' => $list], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});
$app->get('/states/{id}', function (Request $request, Response $response, array $args) {
    try {
        $states = new States();
        $item = $states->one('id=' . $request->getAttribute('id'));
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'item' => $item], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});

$app->get('/items', function (Request $request, Response $response, array $args) {
    try {
        $items = new Items();
        $list = $items->list();
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'list' => $list], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});
$app->get('/items/{id}', function (Request $request, Response $response, array $args) {
    try {
        $items = new Items();
        $item = $items->one('id=' . $request->getAttribute('id'));
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'item' => $item], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});

$app->get('/bills', function (Request $request, Response $response, array $args) {
    try {
        $bills = new Bills();
        $params = $request->getQueryParams();
        $filter = ['1'];
        if (sizeof($params) > 0) {
            if (isset($params['fromDate'])) $filter[] = "date > '" . $params['fromDate'] . "'";
            if (isset($params['stateId'])) $filter[] = "state_id = '" . $params['stateId'] . "'";
        }
        $where = implode(' AND ', $filter);
        $list = $bills->list($where);
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'list' => $list], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});
$app->get('/bills/{id}', function (Request $request, Response $response, array $args) {
    try {
        $bills = new Bills();
        $item = $bills->one('a.id=' . $request->getAttribute('id'));
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'item' => $item], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});
$app->post('/bills', function (Request $request, Response $response, array $args) {
    try {
        $bill = new Bills();
        $bill = $bill->assignOne(Bills::class, $request->getParsedBody());
        $item = $bill->save($bill);
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'item' => $item], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});

$app->get('/bill_items', function (Request $request, Response $response, array $args) {
    try {
        $billItems = new BillItems();
        $params = $request->getQueryParams();
        $filter = ['1'];
        if (sizeof($params) > 0) {
            if (isset($params['billId'])) $filter[] = "bill_id='" . $params['billId'] . "'";
            if (isset($params['itemId'])) $filter[] = "item_id='" . $params['itemId'] . "'";
        }
        $where = implode(' AND ', $filter);
        $list = $billItems->list($where);
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'list' => $list], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});
$app->post('/bill_items', function (Request $request, Response $response, array $args) {
    try {
        $billItems = new BillItems();
        $billItems = $billItems->assignOne(BillItems::class, $request->getParsedBody());
        $item = $billItems->save($billItems);
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'item' => $item], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});

$app->delete('/bill_items/{id}', function (Request $request, Response $response, array $args) {
    try {
        $bills = new BillItems();
        $item = $bills->one('a.id=' . $request->getAttribute('id'));
        $bills->delete('id=' . $request->getAttribute('id'));
    } catch (PDOException $e) {
        return Tools::returnError($response, $e);
    }
    $response->getBody()->write(json_encode(['error' => false, 'item' => $item], JSON_UNESCAPED_UNICODE));
    return $response
        ->withHeader('content-type', 'application/json')
        ->withStatus(200);
});


$app->run();