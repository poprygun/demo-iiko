<?php

namespace App\Models;

use Exception;
use PDO;
use PDOException;
use stdClass;

class Base
{
    private $db;
    private $tableName;
    private $sqlSelect;
    private $sqlDelete;

    public function __construct($tableName)
    {
        $db = new Db();
        $conn = $db->connect();
        $this->db = $conn;
        $this->tableName = $tableName;
        $this->sqlSelect = 'SELECT a.* FROM ' . $this->tableName . ' a WHERE :where';
        $this->sqlDelete = 'DELETE FROM ' . $this->tableName . ' WHERE :where';
    }

    public function assignOne($className, $properties)
    {
        if (!$properties) return new stdClass();
        $res = new $className($this->tableName);
        foreach ($properties as $key => $value) {
            $res->{$key} = $value;
        }
        return $res;
    }

    /**
     * @throws Exception
     */
    public function assignList($className, array $items = array()): array
    {
        $res = [];
        foreach ($items as $item) {
            $itm = $this->assignOne($className, $item);
            $res[] = $itm;
        }
        return $res;
    }

    /**
     * @throws Exception
     */
    public function list($where = '1'): array
    {
        $sql = preg_replace(['/:where/'], $where, $this->sqlSelect);
        $prep = $this->db->prepare($sql);
        try {
            $prep->execute();
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage());
        }
        $res = $prep->fetchAll(PDO::FETCH_OBJ);
        return $this->assignList(Base::class, $res);
    }

    public function one($where = '1'): object
    {
        $sql = preg_replace(['/:where/'], $where, $this->sqlSelect);
        $prep = $this->db->prepare($sql);
        $prep->execute();
        $res = $prep->fetch(PDO::FETCH_OBJ);
        return $this->assignOne(Base::class, $res);
    }


    private function getCols()
    {
        $q = $this->db->prepare("DESCRIBE " . $this->tableName);
        $q->execute();
        return $q->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * @throws Exception
     */
    public function save($data): object
    {
        $keysArr = [];
        $valsArr = [];
        $updValsArr = [];
        $cols = $this->getCols();

        foreach ($data as $key => $value) {
            if ($key == 'id' || !in_array($key, $cols)) continue;
            $keysArr[] = $key;
            $valsArr[] = "'" . $value . "'";
            $updValsArr[] = $key . '=' . "'" . $value . "'";
        }
        $keys = implode(',', $keysArr);
        $vals = implode(',', $valsArr);
        $updVals = implode(',', $updValsArr);
        if ($updVals != '') {
            $sql = isset($data->id)
                ? 'UPDATE ' . $this->tableName . ' SET ' . $updVals . ' WHERE id=' . $data->id
                : 'INSERT INTO ' . $this->tableName . '(' . $keys . ') VALUES (' . $vals . ')';
            $prep = $this->db->prepare($sql);
            $prep->execute();
        }
        return isset($data->id)
            ? $this->one('a.id=' . $data->id)
            : $this->one('a.id=' . $this->db->lastInsertId());
    }

    public function delete($where = 'false'): object
    {
        $res = $this->one($where);
        $sql = preg_replace(['/:where/'], $where, $this->sqlDelete);
        $prep = $this->db->prepare($sql);
        $prep->execute();
        return $res;
    }

    public function setSqlSelect(string $sqlSelect): void
    {
        $this->sqlSelect = $sqlSelect;
    }
}