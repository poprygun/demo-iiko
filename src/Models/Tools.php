<?php

namespace App\Models;

use Exception;
use PDOException;
use Psr\Http\Message\ResponseInterface as Response;

class Tools
{
    public static function returnError(Response $response, \Exception $e): Response
    {
        $error = array(
            "error" => true,
            "message" => iconv('cp1251', 'utf-8', $e->getMessage())
        );
        $response->getBody()->write(json_encode($error, JSON_UNESCAPED_UNICODE));
        return $response
            ->withHeader('content-type', 'application/json')
            ->withStatus(500);
    }

    /**
     * @return void
     * @throws Exception
     */
    public static function initDB()
    {
        $conn = new DB();
        $db = $conn->connect();
        $db->beginTransaction();
        try {
            //language=sql
            $db->query(<<<'EOF'
CREATE DATABASE IF NOT EXISTS `demo-iiko` DEFAULT CHARACTER SET utf8;
USE `demo-iiko`;

DROP TABLE IF EXISTS `bill_items`;
DROP TABLE IF EXISTS `items`;
DROP TABLE IF EXISTS `bills`;
DROP TABLE IF EXISTS `states`;

CREATE TABLE `items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `cost` FLOAT(10,2) DEFAULT 0,
  PRIMARY KEY (`id`)
);

CREATE TABLE `states` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `bills` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `num` varchar(100) NOT NULL,
  `state_id` bigint(20) DEFAULT 1,
  `date` timestamp DEFAULT NOW(),
  `discount` FLOAT(10,2) default 0,
  PRIMARY KEY (`id`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `bills_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `bill_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bill_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) NOT NULL, 
  `amount` int(11) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `bill_id` (`bill_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `bill_items_ibfk_1` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bill_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

insert  into `items`(`id`,`name`, `cost`) 
values (1,'Item #1', 1),(2,'Item #2', 2),(3,'Item #3', 3),(4,'Item #4', 4),(5,'Item #5', 5);

insert  into `states`(`id`,`name`) 
values (1,'New'),(2,'Paid'),(3,'Deleted');

insert  into `bills`(`id`,`num`,`state_id`, `discount`, `date`) 
values (1,'Bill #1',1, 10, '2020-01-01'),(2,'Bill #2',1, 15, '2021-01-02'),(3,'Bill #3',2, 5, '2020-02-01'),(4,'Bill #4',1, 5, '2020-04-01'),(5,'Bill #5',2, 10, '2021-03-03'),(6,'Bill #6',3, 5, '2020-05-01');

insert  INTO `bill_items`(`id`,`item_id`,`amount`,`bill_id`) 
values (1,1,1,1),(2,2,2,1),(3,1,1,2),(4,2,1,2),(5,3,1,2),(6,4,1,3),(7,5,1,3),(8,1,1,4),(9,2,1,4),(10,3,1,4),(11,1,1,5),(12,4,1,6);
EOF
            );
        } catch (PDOException $e) {
            $db->rollBack();
            throw new PDOException($e);
        }
        $db->commit();
    }
}