<?php

namespace App\Models;

use \PDO;

class DB
{
    private $host = 'localhost';
    private $port = '33306';
    private $user = 'root';
    private $pass = '';
    private $dbname = 'demo-iiko';

    public function connect(): PDO
    {
        $conn_str = "mysql:host=$this->host;dbname=$this->dbname;port=$this->port";
        $conn = new PDO($conn_str, $this->user, $this->pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $conn;
    }
}