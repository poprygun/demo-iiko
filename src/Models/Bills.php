<?php

namespace App\Models;

use Exception;
use PDO;

class Bills extends Base
{
    public function __construct()
    {
        parent::__construct('bills');
    }

    /**
     * @throws Exception
     */
    public function one($where = '1'): object
    {
        $this->setSqlSelect(<<<'EOF'
        SELECT a.*, SUM(c.cost*b.amount) AS summ, SUM(c.cost*b.amount)/100*(100-a.discount) AS summ_with_discount
        FROM bills a 
        JOIN bill_items b ON a.id=b.bill_id
        JOIN items c ON b.bill_id=a.id AND b.item_id=c.id
        WHERE :where; 
        EOF
        );
        $res = parent::one($where);

        $items = new States();
        $res->{'state'} = $items->one('id=' . $res->state_id);

        $items = new BillItems();
        $res->{'bill_items'} = $items->list('bill_id=' . $res->id);

        return $res;
    }
}