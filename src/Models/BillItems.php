<?php

namespace App\Models;

class BillItems extends Base
{
    public function __construct()
    {
        parent::__construct('bill_items');
    }

    public function list($where = '1'): array
    {
        $this->setSqlSelect(<<<'EOF'
        SELECT b.*, c.name, c.cost
        FROM bills a
        JOIN bill_items b on a.id = b.bill_id
        JOIN items c on c.id = b.item_id
        WHERE :where
        EOF
        );
        return parent::list($where);
    }

}