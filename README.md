## DEMO-IIKO

Demo PHP-based app

------------------

### Installation

Clone repository to your project folder and install dependencies:

```shell
mkdir app
git clone  git@gitlab.com:poprygun/demo-iiko.git ./
composer install
```

Set your DB connection parameters at `src/Models/DB`:

```php
    ...
    private $host = 'localhost';
    private $port = '3306';
    private $user = 'root';
    private $pass = 'password';
    private $dbname = 'demo-iiko';
    ...
```

### Usage

Serve app by running:

```shell 
php -S localhost:8000 -t public/
```

or set up your custom web-server such as LAMP to serve `public` directory.

## Endpoints

Every of endpoints are defined with test data and covered by tests in [PostMan](https://www.postman.com/downloads/) and can be loaded by this [JSON](https://www.getpostman.com/collections/c8850caff3c98dbefc70)
[RECOMMENDED]

Anyway simple routes description next by here:

- `GET /`: Home path with about text
- `GET /init`: [Re]Creates database with demo data
- `GET /states`: States list
- `GET /states/:id`: One State by its ID
- `GET /items`: Items list
- `GET /items/:id`: One Item by its ID
- `GET /bills`: Bills list
- `GET /bills/:id`: One **detailed** Bill by its ID (with state, items, and calculated sums)
- `GET /bills/?fromDate=2021-01-01`: Bills list filtered by date more than given *fromDate*
    - Sample values: `2021-01-01` or `2021-01-01 08:00:00` or `2021-01-01 08:00`
- `GET /bills/?stateId=1`: Bills list filtered by given *stateId*
- `GET /bills/?fromDate=2021-01-01&stateId=1`: Any combination of previous filters works too
- `POST /bills`: Upsert Bill fields by posted json data
    - Sample update: `{"id":"1", "num": "Bill", "discount":"5"}` or `{"id":"2", "discount":"10"}`
    - Sample insert: `{"num": "Bill", "discount":"5"}` or `{"num":"Bill"}`
- `GET /bill_items/?billId=1`: BillItems list filtered by given *billId*
- `GET /bill_items/?itemId=1`: BillItems list filtered by given *itemId*
- `GET /bill_items/?itemId=1&?billId=1`: Any combination of previous filters works too
- `POST /bill_items`: Upsert BillItems fields by posted json data
    - Sample update: `{id: "1", "bill_id":"1", "item_id":"1", "amount": "1"}` or `{"id":"2", "amount":"10"}`
    - Sample insert: `{"bill_id":"1", "item_id":"1", "amount": "1"}` or `{"item_id":"2"}`
- `DELETE /bill_items/:id`: Delete BillItems by its ID

## Credits
##### Made specialty for IIKO test app by Andrew Zolotarev AKA @poprygun.
###### Powered by [Slim](https://www.slimframework.com/) framework, [MySQL](https://www.mysql.com/) database and [Composer](https://getcomposer.org/) libs. 